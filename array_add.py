# -*- coding: utf-8 -*-
#矩阵相加

def array_add(a,b):
   import copy
   c = a.copy()
   for i in range(len(a)):
      for j in range(len(a[0])):
         c[i][j] = a[i][j] + b[i][j]
   print(c)

a = [[-1,3,2],[5,7,-2],[-3,0,1]]
b = [[8,2,-1],[6,4,0],[-2,3,5]]
array_add(a,b)
