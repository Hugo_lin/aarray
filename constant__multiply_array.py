# -*- coding: utf-8 -*-
#矩阵乘常量

def constant_multiply_array(a,b):
   import copy
   c = a.copy()
   for i in range(len(a)):
      for j in range(len(a[0])):
         c[i][j] = a[i][j] * b
   print(c)
if __name__ == '__main__':
   a = [[1,0,2,4],[-1,3,1,7]]
   b = 3
   constant_multiply_array(a,b)
