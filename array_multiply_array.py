# -*- coding: utf-8 -*-
#矩阵乘矩阵的实现
         
def array_multiply_array(a,b):
   if len(a) != len(b[0]):
      raise ValueError('输入的第一个矩阵的行数与第二个矩阵列数不相等')
   c =[]
   for i in range(len(a)):
      c.append([0])
   for i in range(len(c)):
      for j in range(len(b[0])-1):
         c[i].append(0)

   for i in range(len(a)):
      for j in range(len(b[0])):
         for k in range(len(b[0])):
            c[i][j] =c[i][j] + a[i][k] * b[k][j]
   print (c)      
a = [[1,3,2],[1,0,0],[1,2,2]]
b = [[0,0,2],[7,5,0],[2,1,1]]
array_multiply_array(a,b)
