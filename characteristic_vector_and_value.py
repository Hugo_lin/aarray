# -*- coding: utf-8 -*-
#求N阶方阵特征值和特征向量

import copy
import sympy
#定义一个求方阵行列式的函数
def arraydet(a):
   if len(a)<=0:
      return None
   elif len(a) == 2:
      return a[0][0]*a[1][1]-a[0][1]*a[1][0]
   if len(a) != len(a[0]):
      raise ValueError('输入的不是n阶矩阵')
   det = 0
   for i in range(len(a)):
      m = [[row[j] for j in range(len(a)) if j != i] for row in a[1:]]
      det = det + (-1)**(i) * a[0][i] * arraydet(m)
   return det

#定义求非齐次方程的解的函数
def nonhomogeneous(a,b):
   D = arraydet(a)
   if D == 0 :
      raise ValueError('方程无解或有两个不同的解')
   else :
      x = []
      for i in range (len(b)):
         c = copy.deepcopy(a)
         for j in range (len(b)):
            c[j][i] = b[j][0]      #利用克莱姆法则替换对应列的值
         x.append([arraydet(c) / D])  #求出方程解的集合
      return x
   
#定义求特征值和特征向量的值
def characteristic(a):
   x = sympy.Symbol('x')
   c = copy.deepcopy(a)
   for i in range(len(a)):
      c[i][i] = c[i][i] - x
   fx = arraydet (c)
   characteristic_value = sympy.solve(fx,x)
   for i in range(len(characteristic_value)):
      print ('方阵特征值%d为：' % int(i+1),characteristic_value[i])
   for m in characteristic_value :
      d = copy.deepcopy(a)
      for i in range(len(a)):
         d [i][i] = d[i][i] - m
      e = [[0]]*len(d)
      characteristic_vector = nonhomogeneous(d,e)
      print(characteristic_vector)
if __name__ == '__main__':
   a = [[2,1,-5,1],[1,-3,0,-6],[0,2,-1,2],[1,4,-7,6]]
   b = [[-2,1,1],[0,2,0],[-4,1,3]]
   c = [[3,-1],[-1,3]]
   characteristic(c)

   
