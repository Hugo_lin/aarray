# -*- coding: utf-8 -*-
#行列式功能的实现
def arraydet(a):
   if len(a)<=0:
      return None
   elif len(a) == 2:
      return a[0][0]*a[1][1]-a[0][1]*a[1][0]
   if len(a) != len(a[0]):
      raise ValueError('输入的不是n阶矩阵')
   det = 0
   for i in range(len(a)):
      m = [[row[j] for j in range(len(a)) if j != i] for row in a[1:]]
      det = det + (-1)**(i) * a[0][i] * arraydet(m)
   return det
   
if __name__ == '__main__':
   a = [[1,1,1,1],[1,2,3,1],[1,5,1,1],[1,2,3,4]]
   #a = [[1,1,1],[1,2,3],[1,5,1]]
   print(arraydet(a))

   
